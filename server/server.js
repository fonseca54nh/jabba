const express         = require( 'express' )
const app             = express()
const server          = require('http').createServer(app)
const WebSocket       = require( 'ws' )
const app2            = express()
const server2         = require('http').createServer(app2)
const { MongoClient } = require( 'mongodb' )
const url             = 'mongodb://localhost:27017'
const mongoclient     = new MongoClient( url )
const dbName          = 'jabbaDB'
const wss             = new WebSocket.Server( { server } )

server.listen( 10000, (ws) => {console.log( "Server listens on 10000" ) })

var path = require('path')

app.use(express.static(__dirname + '/style'));
app.use(express.static( path.resolve( 'client/' ) ));
app.use(express.static( __dirname ) )
app.get('/', function (req, res) { res.sendFile( path.resolve('client/index.html') ) })

server2.listen( 4000, (ws) => {console.log( 'word bank connected' )} )
app2.use(express.static( __dirname + '/style' ));
app2.use(express.static( __dirname + "/add/" ));
app2.get('/', function (req, res) { res.sendFile( __dirname + '/add/' ) })
app2.use(express.urlencoded({ extended: true }))
app2.post( '/add'    , ( req , res ) => { insertWord(req.body.word); res.send(req.body); } )
app2.post( '/rem'    , ( req , res ) => { removeWord(req.body.word); res.send(req.body); } )
app2.get ( '/att'    , ( req , res ) => { wordlist().then( allWords => res.send( JSON.stringify( allWords ) ) ) } )

www = []

async function wordlist()
{
	await mongoclient.connect();
	const db    = mongoclient.db( dbName )
	const words = db.collection( 'words' )
	app2.locals.collection = words
	word = await words.find().toArray()
	www = []
	for( i = 0; i < word.length; ++i ) { www.push( word[i].word ) }
	return www
}

async function insertWord( dword )
{
	await mongoclient.connect();
	const db    = mongoclient.db( dbName )
	const words = db.collection( 'words' )
	app2.locals.collection = words
	word = await words.insert( {word: dword.toLowerCase()} )
	www = []
	for( i = 0; i < word.length; ++i ) { www.push( word[i].word ) }
	return www
}

async function removeWord( dword )
{
	await mongoclient.connect();
	const db    = mongoclient.db( dbName )
	const words = db.collection( 'words' )
	app2.locals.collection = words
	word = await words.remove( {word: dword.toLowerCase()} )
	www = []
	for( i = 0; i < word.length; ++i ) { www.push( word[i].word ) }
	return www
}

wss.onopen = () => { console.log( 'Server opened' ) }


class match
{
	constructor( p1, p2, ri, w, t, c, hw )
	{
		this.player1      = p1,
		this.player2      = p2,
		this.roomID       = ri,
		this.word         = w ,
		this.turn         = t ,
		this.counter      = c ,
		this.hiddenWord   = hw
	}

	getTurn()       { return this.turn       }
	getWord()       { return this.word       }
	getRoomID()     { return this.roomID     }
	getCounter()    { return this.counter    }
	getPlayer1()    { return this.player1    }
	getPlayer2()    { return this.player2    }
	getHiddenWord() { return this.hiddenWord }

	setTurn       (    turn    ) { this.turn       = turn       }
	setWord       (    word    ) { this.word       = word       }
	setRoomID     (   roomID   ) { this.roomID     = roomID     }
	setCounter    (   counter  ) { this.counter    = counter    }
	setPlayer1    (   player1  ) { this.player1    = player1    }
	setPlayer2    (   player2  ) { this.player2    = player2    }
	setHiddenWord ( hiddenWord ) { this.hiddenWord = hiddenWord }
}


// Receiving messages
// receiveUsername
// requestPlayers
// invitePlayer
// inviteResponse
// checkLetter
// quit
// rematch
//
//
// Sending messages
// receiveUsername{ usernameApproval   } => goto lobby
// requestPlayers { connectedPlayers   } => returns user array
// invitePlayer   { sendInvite	       } => send an invite
// inviteResponse { sendInviteResponse } => transition or not, remove onl list
// checkLetter	  { movementResult     } => sends play result 
// checkLetter	  { defeat	       } => send defeat msg
// checkLetter	  { win		       } => send win and def msg
// quit		  { rageQuit	       } => remove from the playerLists
// quit		  { rageQuit	       } => send to rem. user rage quitter
// rematch	  { rematch	       } => send rematch request
//
// Receive Parameters -- client
//
// receiveUsername -> tipo -> name
// requestPlayers  -> tipo 
// invitePlayer	   -> tipo -> from   -> to
// inviteResponse  -> tipo -> from   -> to -> bool
// checkLetter	   -> tipo -> roomID -> letter
// quit		   -> tipo -> roomID
// rematch	   -> tipo -> roomID
//
//
//
//
//
// Send Parameters -- server
//
// usernameApproval    -> type	-> bool
// connectedPlayers    -> type	-> userlist
// sendInvite	       -> type	-> from		-> to
// sendInviteResponse  -> type	-> from		-> to
// movementResult      -> type	-> bool		-> hiddenWord
// defeat	       -> type	
// win		       -> type	-> winner	-> loser
// rageQuit	       -> type	-> quitter	-> toLobby
// rematch	       -> type	-> sendInvite

roomID = 0
errorCounter = 0
wss.on( 'connection', ( ws ) => 
{ 
	ws.id    = ''
	ws.match = ''
	ws.onmessage = ( { data } ) =>
	{
		data = parse( data )
		console.log( data )
		type = data.type

		switch(type)
		{
		case 'receiveUsername':
			usernameApproval      = { type: "usernameApproval" }
			usernameApproval.bool = true
			ws.id = data.username
			exist = foreach( wss, [ ws.id ] )
			if( exist.length > 1 ) usernameApproval.bool = false
			sendToClient( usernameApproval,ws )
			break

		case 'requestPlayers':
			connectedPlayers = { type: 'connectedPlayers' }
			connectedPlayers.userlist = forall( wss, ws.id )
			sendToClient( connectedPlayers, ws )
			break
		case 'invitePlayer':
			sendInvite      = { type: 'sendInvite' }
			sendInvite.from = data.from
			sendInvite.to   = data.to
			client = foreach( wss, [ data.to ] )
			sendToClient( sendInvite, client[0] )
			break
		case 'inviteResponse':
			sendInviteResponse = { type: 'sendInviteResponse' }
			sendInviteResponse.from = data.from
			sendInviteResponse.to   = data.to
			sendInviteResponse.bool = data.bool
			clients = foreach( wss, [ data.from, data.to ] )
			if( data.bool && clients.length === 2 )
			{
				console.log( "hello there" )
				roomID++;
				wordlist().then( allWords => 
				{
					rr = Math.floor((Math.random() * allWords.length) );
					m1 = new match( data.from, data.to, roomID, 
					allWords[rr], data.to, errorCounter, 
					hideWord( allWords[rr] )  )

					sendInviteResponse.hiddenWord = m1.hiddenWord
					sendInviteResponse.turn       = m1.turn
					clients[0].match = m1
					clients[1].match = m1

					sendToClient( sendInviteResponse, clients[0] )
					sendToClient( sendInviteResponse, clients[1] )
					
					wss.clients.forEach( function each( client )
					{
						connectedPlayers = { type: 'connectedPlayers' }
						connectedPlayers.userlist = forall( wss, client.id )
						sendToClient( connectedPlayers, client )

					})
				})
			}
			else
			{
				sendInviteResponse.bool = false
				if( clients.length === 2 )
				{
					clients[0].match = ''
					clients[1].match = ''
					sendInviteResponse.message = "Invite rejected"
					sendToClient( sendInviteResponse, clients[1] )
				}
				if( clients.length === 1 )
				{
					clients[0].match = ''
					sendInviteResponse.message = "Your opponent was too weak! So I killed him!"
					sendToClient( sendInviteResponse, clients[0] )
				}
			}
			break
		case 'checkLetter':
			flag = 0; flagw = 0; flag2 = 0
			player1 = foreach( wss, [ ws.id ] )
			players = foreach( wss, [ player1[0].match.player2, player1[0].match.player1 ] )

			if( getTurn( players[0] ) === getPlayer1( players[0] ) )
			{
				setTurn( players[0], players[0].match.player2)
				setTurn( players[1], players[0].match.player2)
			}
			else
			{
				setTurn( players[0], players[0].match.player1)
				setTurn( players[1], players[0].match.player1)
			}

			checkLetter = { type: 'checkLetter' }
			for( let i = 0; i < player1[0].match.word.length; ++i )
			{
				console.log( player1[0].match.word[i] )
				if( data.letter === player1[0].match.word[i])
				{
					console.log( "olha eu aqui" )
					flag += 1	
					let hw = dummyWord( data.letter, getHiddenWord( player1[0] ), i )
					console.log( hw )
					players[0].match.setHiddenWord( hw )
					players[1].match.setHiddenWord( hw )
				}
				if( player1[0].match.hiddenWord[i] !== "_" ) flag2 += 1
			}
			console.log( players[0].match.hiddenWord )

			
			if( flag === 0 ) { players[0].match.counter += 1 }

			if( flag2 === player1[0].match.word.length ) flagw = 1

			players = foreach( wss, [ player1[0].match.player1, player1[0].match.player2 ] )

			checkLetter.letter	 = data.letter
			checkLetter.hiddenWord   = getHiddenWord ( players[0] )
			checkLetter.errorCounter = getCounter    ( players[0] )
			checkLetter.turn         = getTurn       ( players[0] )
			checkLetter.flagw        = flagw

			sendToClient( checkLetter, players[0] )
			sendToClient( checkLetter, players[1] )
			break
		case 'quit':
			player1 = foreach( wss, [ ws.id ] )
			players = foreach( wss, [ player1[0].match.player2, player1[0].match.player1 ] )
			ragequit = { type: 'ragequit' }
			if( getPlayer1( players[0] )  === ws.id ) 
			{ 
				sendToClient( ragequit, players[0] );
				players[0].match = '' 
				players[1].match = '' 
			}
			else 
			{ 
				sendToClient( ragequit, players[1] ) 
				players[0].match = '' 
				players[1].match = ''; 
			}
			break
		case 'rematch':
			player1 = foreach( wss, [ ws.id ] )
			players = foreach( wss, [ player1[0].match.player2, player1[0].match.player1 ] )
			sendInvite      = { type: 'sendInvite' }
			sendInvite.from = data.from
			sendInvite.to   = data.to


			if( getPlayer1( players[0] ) === ws.id )
			{
				sendInvite.from = getPlayer1( players[0] )
				sendInvite.to   = getPlayer2( players[0] )
				sendToClient( sendInvite, players[0] )
			}
			else
			{
				sendInvite.from = getPlayer2( players[0] )
				sendInvite.to   = getPlayer1( players[0] )
				sendToClient( sendInvite, players[1] )
			}
			break
		case 'toLobby':
			toLobby = { type: 'toLobby' }
			if( ws.match !== '' )
			{
				player1 = foreach( wss, [ ws.id ] )
				players = foreach( wss, [ player1[0].match.player2, player1[0].match.player1 ] )
				if( players.length === 2 ) 
				{ 
					sendToClient( toLobby, players[1] ) 
					players[1].match = ''
				}

				sendToClient( toLobby, players[0] )
				players[0].match = ''
			}
			else
			{
				sendToClient( toLobby, ws )
			}
			break
		default: break
		}
	}

	ws.on( "close", () => 
	{
		player1 = []
		if( ws.match.player1 === ws.id )
			player1 = foreach( wss, [ ws.match.player2 ] )
		else
			player1 = foreach( wss, [ ws.match.player1 ] )
		if( player1[0] )
		{
			player1[0].match = ''
			ragequit = { type: 'ragequit' }
			sendToClient( ragequit, player1[0] )
		}

		wss.clients.forEach( function each( client )
		{
			connectedPlayers = { type: 'connectedPlayers' }
			connectedPlayers.userlist = forall( wss, client.id )
			sendToClient( connectedPlayers, client )
		})
	}) 

})

function parse( a ) 	      { return JSON.parse( a )        }
function sendToClient( a, b ) { b.send( JSON.stringify( a ) ) }
function foreach( a, b )      
{ 
	vet = [] 
	for( i = 0; i < b.length; i++ )
	{
		a.clients.forEach( function each( client )
		{
			if( b[i] === client.id ) vet.push( client )
		})
	}
	return vet
}

function forall( a,b )      
{ 
	vet = [] 
	a.clients.forEach( function each( client )
	{
		console.log( client.id )
		if( b != client.id ) { if( client.match === '' ) { vet.push( client.id ) } }
	})
	return vet
}

function hideWord( word )
{
	hiddenWord = ''
	for( i = 0; i < word.length; ++i ) hiddenWord += '_'
	return hiddenWord
}

function dummyWord( letter, word, position )
{
	dummy = []; dword = ''
	for( i = 0; i < word.length; ++i  ) { dummy.push( word[ i ] ) }
	dummy[position] = letter
	for( i = 0; i < word.length; ++i  ) { dword += dummy[ i ] }
	return dword
}

function getTurn       ( player ) { return player.match.turn       }
function getWord       ( player ) { return player.match.word       }
function getRoomID     ( player ) { return player.match.roomID     }
function getCounter    ( player ) { return player.match.counter    }
function getPlayer1    ( player ) { return player.match.player1    }
function getPlayer2    ( player ) { return player.match.player2    }
function getHiddenWord ( player ) { return player.match.hiddenWord }

function setTurn       ( player, turn       ) { player.match.turn       = turn       }
function setWord       ( player, word       ) { player.match.word       = word       }
function setCounter    ( player, counter    ) { player.match.counter    = counter    }
function setHiddenWord ( player, hiddenWord ) { player.match.hiddenWord = hiddenWord }
