let ws = new WebSocket('ws://localhost:10000')

playersDiv = document.querySelector( '#playersDiv' )
send    = document.querySelector( '#send' )

function sendToServer( a )      { ws.send( JSON.stringify( a ) )              }
function parse(a)               { return JSON.parse( a )                      }
function select(a)              { return document.querySelector( '#' + a )    }
function selectClass(a)         { return document.getElementsByClassName( a ) }
function show(a)                { select( a ).style = 'display: block;'       }
function hide(a)                { select( a ).style = 'display: none;'        }
function style(a,b)             { select( a ).style = b                       }
function setInner(a,b)          { select( a ).innerText = b                   }
function getInner(a)            { return select( a ).innerText                }
function getName()              { return select( "uname" ).value 	      }
function getInnerFromClass(a,b) { return selectClass( a )[b].innerText        }
function disable( a )           { select( a ).disabled = true; select( a ).style = "background-color:grey;" }
function enableAll()		{ 
	l = selectClass( "letter" )
	console.log( l )
	for( let i = 0; i < l.length; ++i )
		{ l[i].disabled = false; l[i].style = "background-color: none;" }
}

ws.onopen = () => { inputUserName() };

ws.onmessage = ( { data } ) => 
{ 
	data = parse( data )
	console.log( data )
	type = data.type

	switch( type )
	{
		case 'usernameApproval':
			if( data.bool === true ) { hide( 'inviteDiv' ); hide( "welp" ); hide( "send" ); hide( "uname" ) }
			else { alert( 'username already taken!' ) }
			break
		case 'connectedPlayers':
			playersDiv.innerText = ''

			for( i = 0; i < data.userlist.length; ++i )
			{
				btn = document.createElement('button');
				btn.setAttribute( "id", "#user" + data.userlist[i] )
				btn.setAttribute( "class", "players" )
				btn.innerHTML = data.userlist[i];
				//playersDiv.style ="playersDiv", "border: 2px solid rgba( 20,20,20,0 );"
				//btn.style = " display:grid; grid-template-columns: auto auto auto; width: 30%;  text-align: center;"
				playersDiv.appendChild(btn);
				updatePlayers()
			}
			break
		case 'sendInvite':
			inviteScreen( data.from )
			break	
		case 'sendInviteResponse':
			if( data.bool ) 
			{ 	
				gameScreen( data.hiddenWord ) 
				if( data.turn === select( "uname" ).value ) turn( true )
				else turn( false )

			}
			else rejected( data.message )
		case 'checkLetter':
			switch( data.flagw )
			{
			case 0:
				if( data.turn === getName() ) turn( true )
				else turn( false )
				setInner( "word", data.hiddenWord )
				setInner( "eText", data.errorCounter )
				disable( data.letter )
				switch( data.errorCounter )
				{
				case 0:
					hide( "image0" )
					break
				case 1:
					hide( "image0" )
					show( "image1" )
					break
				case 2:
					hide( "image1" )
					show( "image2" )
					break
				case 3:
					hide( "image2" )
					show( "image3" )
					break
				case 4:
					hide( "image3" )
					show( "image4" )
					break
				case 5:
					hide( "image4" )
					defeat()
					break
				default: break
				}
				break
			case 1:
				hide( "image0" )
				hide( "image1" )
				hide( "image2" )
				hide( "image3" )
				hide( "image4" )
				hide( "image" )
				setInner( "word", data.hiddenWord )
				disable( data.letter )
				win()
				break
				
			default:
				break
			}
			break	
		case 'ragequit':
			ragequit()
			hide( "inviteDiv" )
			break
		case 'toLobby':
			lobbyScreen(); 
			break
		default: break
	}
}

function inputUserName()
{
	sendUsername = { type: 'receiveUsername' }

	setInner( "title", "Select a username" )
	show ( "title"     )
	show ( "inviteDiv" )
	hide ( "accept"    )
	hide ( "cancel"    )
	hide ( "welp"      )
	hide ( "quit"      )
	hide ( "lobby"     )
	select( "send" ).onclick = () => 
	{
		sendUsername.username = getName()
		sendToServer( sendUsername )
		show( "lobby" )
		setInner( "welcome", "Welcome " + getName() + " to Jabba Forca" )
	}

}

function updatePlayers()
{
	for( let i = 0; i < selectClass( "players" ).length; ++i )
	{
		selectClass( "players" )[i].onclick = () => 
		{ 
			invitePlayer = { type: 'invitePlayer' };
			invitePlayer.from = getName()
			invitePlayer.to = getInnerFromClass( "players", i )
			sendToServer( invitePlayer ) 
		}
	}
}

function showInvite( player )
{
	setInner( "title", "Received invite from: " + player )
	show( "accept"    )
	show( "cancel"    )
	show( "title"     )
	show( "inviteDiv" )
}

function hideInvite()
{
	hide( "accept"    )
	hide( "cancel"    )
	hide( "title"     )
	hide( "inviteDiv" )
}

function inviteScreen( player )
{
	showInvite( player )
	inviteResponse = { type: "inviteResponse" }
	inviteResponse.from = select( "uname" ).value
	inviteResponse.to   = player
	select( "accept" ).onclick = () =>
	{
		inviteResponse.bool = true
		sendToServer( inviteResponse )
		hideInvite()
	}
	select( "cancel" ).onclick = () =>
	{
		inviteResponse.bool = false
		sendToServer( inviteResponse )
		hideInvite()
		lobbyScreen()
	}
	hide( "welp" )
	hide( "welp2" )
}

function gameScreen( word )
{
	enableAll()
	hide( "image0" )
	hide( "image1" )
	hide( "image2" )
	hide( "image3" )
	hide( "image4" )
	hide( "lobby" )
	hide( "inviteDiv" )
	hide( "finalResult" )
	show( "game"  )
	show( "quit" )
	setInner( "eText", "0" )
	setInner( "word", word )
	cletter();
}

function lobbyScreen() 
{ 
	setInner("playersDiv", "");
	updatePlayers() ;
	hide( "game" ); show( "lobby"  ); hide( "quit" ) 
}

function rejected( mess )
{
	hideInvite()
	if( mess === "Your opponent was too weak! So I killed him!" )
	{
		setInner( "title", mess )
		show( "darth" )
	}
	else
	{
		setInner( "title", mess )
		hide( "darth" )
	}
	show( "title"     )
	show( "inviteDiv" )
	show( "welp"      )
	select( "welp" ).onclick = () => { lobbyScreen(); hide( "inviteDiv" ) }
	select( "welp2" ).onclick = () => { lobbyScreen(); hide( "inviteDiv" ) }
}

function turn( turner )
{
	if( turner ) { hide("ree");   setInner( "turnTitle", "Play Shall You!" )     }
	else         { show( "ree" ); setInner( "turnTitle", "Play Shall You Not!" ) }
	show( "yoda" )
	show( "turn" )
}

function cletter()
{
	for( let i = 0; i < selectClass( "letter" ).length; ++i )
	{
		selectClass( "letter" )[i].onclick = () =>
		{
			checkLetter = { type: 'checkLetter' }
			checkLetter.letter = getInnerFromClass( "letter", i )
			sendToServer( checkLetter )
		}
	}
}

function defeat()
{
	setInner( "winTitle", "You Have Frozen Han!")
	hide ( "wimage"      )
	show ( "image"       )
	hide ( "yoda"        )
	hide ( "darth"       )
	hide ( "welp"        )
	hide ( "welp2"       )
	show ( "goLobby"     )
	show ( "rematch"     )
	show ( "winTitle"    )
	show ( "finalResult" )
}

function win()
{
	setInner( "winTitle", "Han Always Shoots First!")
	show ( "wimage"      )
	hide ( "image"       )
	hide ( "yoda"        )
	hide ( "darth"       )
	hide ( "welp"        )
	hide ( "welp2"       )
	show ( "goLobby"     )
	show ( "rematch"     )
	show ( "winTitle"    )
	show ( "finalResult" )
}

function ragequit()
{
	setInner( "winTitle", "Your partner was weak! So I killed him!")
	hide ( "wimage"      )
	hide ( "image"       )
	hide ( "yoda"        )
	show ( "darth"       )
	show ( "welp2"       )
	hide ( "goLobby"     )
	hide ( "rematch"     )
	show ( "winTitle"    )
	show ( "finalResult" )
	select( "welp2" ).onclick = () => { lobbyScreen(); hide( "inviteDiv" ) }
}

select( "updateUsers" ).onclick = () => { requestPlayers = { type: 'requestPlayers' }; sendToServer( requestPlayers ) }

select( "quit" ).onclick = () => { lobbyScreen(); quit = { type: 'quit' }; sendToServer( quit ) }

select( "rematch" ).onclick = () => { rematch = { type: "rematch" }; sendToServer( rematch ) }
select( "goLobby" ).onclick = () => {  toLobby = { type: 'toLobby' }; sendToServer( toLobby ); }


